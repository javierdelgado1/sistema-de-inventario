-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-10-2014 a las 23:25:54
-- Versión del servidor: 5.6.15-log
-- Versión de PHP: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistemainventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha1` date NOT NULL,
  `fecha2` date NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo` varchar(11) NOT NULL,
  `codigo` varchar(25) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `sserial` varchar(50) NOT NULL,
  `proveedor` int(11) NOT NULL,
  `ubicacion` varchar(300) NOT NULL,
  `area` varchar(25) NOT NULL,
  `ordendecompra` varchar(25) NOT NULL,
  `comentario` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`id`, `fecha1`, `fecha2`, `cantidad`, `costo`, `codigo`, `descripcion`, `sserial`, `proveedor`, `ubicacion`, `area`, `ordendecompra`, `comentario`) VALUES
(6, '2014-10-17', '2014-10-17', 2, '2014', '123xxxx', 'hola mundo', '123123', 4, 'sfsdfdsf', '1,3', 'v-19940339', 'Soy un comentario2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(25) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `responsable` varchar(25) NOT NULL,
  `ubicacion` varchar(300) NOT NULL,
  `telefono` int(12) NOT NULL,
  `rif` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `codigo`, `descripcion`, `responsable`, `ubicacion`, `telefono`, `rif`) VALUES
(1, '123', ' descripcion', 'Javier Delgado', 'njkjnjk', 2147483647, 'v-19940338'),
(3, 'sdfsdf', ' descripcion', 'Javier Delgado', 'dgdfgdfgdfgdfg', 2147483647, 'v-19940338'),
(4, '1111', ' descripcion', 'Javier Delgado', 'sdfsdfsdf', 2147483647, 'v-19940338');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `cedula` varchar(8) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `correo` varchar(75) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `habilitado` tinyint(1) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `creador` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correo` (`correo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `cedula`, `apellido`, `correo`, `telefono`, `direccion`, `tipo`, `habilitado`, `contrasena`, `creador`) VALUES
(19, 'javier', '', 'delgado', 'cheche3jh38@gmail.com', '', '', 'Gerente', 1, '12121212', 0),
(20, 'modificado', '', 'delgadozxczxcz', 'chejbkjhche338@gmail.com', '', '', 'Gerente', 0, '12121212', 0),
(17, 'javier', '', 'delgado', 'chece33@gmail.com', '', '', 'Gerente', 0, '12121212', 0),
(18, 'javier', '', 'delgado', 'cheche338@gmail.com', '', '', '1', 1, '123', 0),
(21, 'Nuevo', '', 'apellidoNuevo', 'Javier@gmail.com', '', '', '0', 1, '123', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
