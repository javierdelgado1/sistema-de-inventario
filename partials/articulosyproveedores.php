<?php
	session_start(); 
	if(!isset($_SESSION['nombre'])){
		header ("Location: index.html");
	}
?>

<div class="panel panel-default">
 <!--  <div class="panel-heading">Menu Principal</div> -->
  		<div class="panel-body">
			

			<div class="container">
			    <ul class="nav nav-pills">
			       <li id="minsertar" class="active"><a href="#" id="insertar"> Insertar</a></li>
			       <li id="mconsultar"><a href="#" id="consultar"> Consultar</a></li>
			     
		       		<li id="mmodificar" style="display:none;"><a href="#" id="modificar"> Modificar</a></li>
		      		<li id="meliminar"  style="display:none;"><a href="#" id="eliminar"> Eliminar</a></li>
			     
			       
			      
			       <li ><a href="#" id="cerrarsesion"> Cerrar sesion</a></li>   
			    </ul>
			    
			</div> 
			<hr> <br>
			<div class="row">
			    <div class="col-lg-12">			        
			        <ol class="breadcrumb">
			            <li><a href="panel.php">Inicio</a>
			            </li>
			            <li class="active">Articulos y proveedores</li>
			        </ol>
			        <div id="labelnombre">
			    		<h1 class="page-header" >Articulos</h1>
			    	</div>
			    	
			    </div>

			</div>
			
				<div class="row">
					 <div class="col-md-6 col-xs-6 col-sm-6">
						 <ul class="nav nav-pills nav-stacked">
							<li id="opcArticulo" class="active"><a href="#" >Articulo</a></li>
			      			 <li id="opcProveedor"><a href="#" >Proveedor</a></li>
						</ul>
					 </div>
					 <div class="col-md-6 col-xs-6 col-sm-6">
					 	<div id="articulo" style="display:block;">
						 	<div id="labelArticulo"><h1>Insertar Nuevo Articulo</h1></div>
						 	<div id="alertasArticulo"></div>
						 	<form role="form" onsubmit="return false;">
						 	   <div class="input-group"  id="buscarArticulo" style="display:none;">								  
								    <input type="text" class="form-control" id="tBuscar" placeholder="Codigo">
								      <span class="input-group-btn">
								        <button class="btn btn-primary" type="button" id="buscar" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
								      </span>			      
								    </div>


					        	<div class="form-group">
								    <label >Codigo</label>
								    <input type="text" class="form-control" id="tcodigo"   >
								  </div>
								  <div class="form-group">
								    <label >Descripcion</label>
								     <textarea class="form-control"  rows="4" cols="60"  id="tdescripcion" ></textarea>			    
								  </div>

								  <div class="form-group">
								    <label >Serial</label>
								    <input type="text" class="form-control" id="tserial"   >
								  </div>
								  
								   <div class="form-group">
								    <label >Proveedor</label>
								    <select class="form-control" id="tproveedor"></select>							    
								  </div>
								  <div class="form-group">
								    <label >Ubicacion</label>
								     <input type="text" class="form-control" id="tUbicacion2"  >
								  </div>
								    <div class="form-group">
									    <label >Area</label>
									     <input type="text" class="form-control" id="tarea" >
								  </div>
							</form>
							<a  href="panel.php"   class="btn btn-default" >Salir</a>
		       					<button type="button" class="btn btn-primary" id="aceptar2">Aceptar</button>
	       					
						 </div>
						 <div id="proveedor" style="display:none;">
						 		<div id="labelProveedor"><h1>Insertar Nuevo Proveedor</h1></div>
						 		<div id="alertasProveedor"></div>
							 	<form role="form" onsubmit="return false;">
							 		<div class="input-group"  id="buscarProveedor" style="display:none;">
									 <input type="text" class="form-control" id="tBuscar2" placeholder="Codigo">
								      <span class="input-group-btn">
								        <button class="btn btn-primary" type="button" id="buscar2" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
								      </span>			      
								    </div>
						        	<div class="form-group">
									    <label >Codigo</label>
									    <input type="text" class="form-control" id="tcodigo2"   >
									  </div>
									  <div class="form-group">
									    <label >Descripcion</label>
									     <textarea class="form-control"  rows="4" cols="60"  id="tdescripcion2"  ></textarea>			    
									  </div>

									  <div class="form-group">
									    <label >Responsable</label>
									    <input type="text" class="form-control" id="tresponsable"   >
									  </div>
									  
									     <div class="form-group">
									    <label >Ubicacion</label>
									     <textarea class="form-control"  rows="4" cols="60"  id="tUbicacion" ></textarea>			    
									  </div>									 
									    <div class="form-group">
									    <label >Telefono</label>
									     <input type="text" class="form-control" id="ttelefono"  >
									  </div>
									   <div class="form-group">
										    <label >Rif</label>
										     <input type="text" class="form-control" id="tRif" >
									  </div>
								</form>
								<a  href="panel.php"   class="btn btn-default" >Salir</a>
		       					<button type="button" class="btn btn-primary" id="aceptar">Aceptar</button>
						  </div>
					 </div>
					
			</div>
			
			<div class="row">

					<div class="col-md-12 col-xs-12 col-sm-12">
								<table class="table"  id="tablaArticulo">
								<hr><br><br>
									<tr>
										<td><b>Orden de Compra</b></td>
										<td><b>Fecha de Adquisicion</b></td>
										<td><b>Fecha de Expedicion de Garantia</b></td>
										<td><b>Cantidad</b></td>
										<td><b>Costo</b></td>
										<td><b>Comentario</b></td>
									</tr>
									<tr>
										<td><input type="text" class="form-control" id="tordencompra" ></td>
										<td><input type="date" class="form-control" id="tfecha"   ></td>
										<td><input type="date" class="form-control" id="tfecha2"   ></td>
										<td><input type="text" class="form-control" id="tcantidad"   ></td>
										<td><input type="text" class="form-control" id="tcosto"   ></td>
										<td><input type="text" class="form-control" id="tcomentario"   ></td>
									</tr>
								</table>
					</div>
			</div>

	   </div>
</div>
