<br>
<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<div class="panel panel-default">
 <!--  <div class="panel-heading">Menu Principal</div> -->
  		<div class="panel-body">
			<div class="row">
			    <div class="col-lg-12">			        
			        <ol class="breadcrumb">
			            <li><a href="panel.php">Inicio</a>
			            </li>
			            <li class="active">Inventario</li>
			        </ol>
			        <h1>Calculo para determinar cantidad y lapso de tiempo</h1>
			        <hr><br>
			    </div>
			</div>

			<!-- <div class="container">
			    <ul class="nav nav-pills">
			       <li><a href="#"> Insertar</a></li>
			       <li><a href="#"> Consultar</a></li>
			       <li><a href="#"> Modificar</a></li>
			       <li><a href="#"> Eliminar</a></li>
			       <li><a href="#"> Cerrar sesion</a></li>   
			    </ul>
			    <h1 class="page-header">Inventario			         
			        </h1>
			</div>  -->

				
				<div class="row">
					 <div class="col-md-6 col-xs-6 col-sm-6">
							<div class="input-group"  id="buscarUsuario" >
								 <input type="text" class="form-control" id="tBuscar" placeholder="Busqueda por Codigo">
							      <span class="input-group-btn">
							        <button class="btn btn-primary" type="button" id="buscar" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
							      </span>			      
						    </div>
						    <div id="alertaBusquedaporCodigo"></div>
						    <br><br><br>
						    <div class="form-group">
							    <label >Codigo</label>
							    <input type="text" class="form-control" id="tcodigo"   >
							 </div>
							 <div class="form-group">
							    <label >Descripcion</label>
							    <input type="text" class="form-control" id="tdescripcion"   >
							 </div>
							 <div class="form-group">
							    <label >Precio de Lista</label>
							    <input type="text" class="form-control" id="tpreciodelista"  >
							 </div>
							 <br>
							 <hr>
							 <br>
							 <div class="form-group">
							    <label >Restricciones de espacio en el almacenamiento</label>
							    <select class="form-control" id="restriccionesdeespacio">
							    	<option value="-1"></option>
							    	<option value="si">Si</option>
							    	<option value="no">No</option>
							    </select>
							    <div id="Restriccionesdeespacio2" style="display: none;">
							    		 <label >Cantidad de Productos involucrados</label>
							   			 <input type="text" class="form-control" id="productosinvolucrados">
							    </div>							    
							 </div>
							<div class="form-group">
							    <label >Tipo de Demanda</label>
							    <select class="form-control" id="tipodedemanda">
							    	<option value="-1"></option>
							    	<option value="horizontal">Horizontal</option>
							    	<option value="Crecienteodecreciente">Creciente o decreciente</option>							    	
							    	<option value="estacional">Estacional</option>
							    	<option value="Probabilistica">Probabilistico</option>							    	
							    </select>
							    <div id="tipodedemanda2" style="display: none;">
							    		 <label >Numero de estaciones</label>
							   			 <input type="text" class="form-control" id="numerodeestaciones">
							   			 <label >Demanda total por estaciones</label>
							   			 <input type="text" class="form-control" id="demandaporestaciones">
							    </div>	
							    <div id="tipodedemanda3" style="display: none;">
							    		 <label id="ltipodemanda" >Media de la demanda  proyectada</label>
							   			 <input type="text" class="form-control" id="tTipodemanda">							   			
							    </div>	
							 </div> 
							 <div class="form-group">
							    <label >Tipo de sistema de control de inventario seleccionado</label>
							    <select class="form-control" id="tipodesistemadecontrolseleccionado">
							    	<option value="-1"></option>
							    	<option value="periodofijo">Periodo Fijo</option>
							    	<option value="Unsolopedido">Un Solo Pedido</option>
							    	<option value="cantidadFija">Cantidad Fija</option>
							    </select>
							    <div id="divperiodofijo" style="display: none;">
							    		 <label id="ltipodemanda" >Tiempo de ciclo entre compras (t)</label>
							   			 <input type="text" class="form-control" id="tiempodeciclo">
							   			 <label id="ltipodemanda" >Inventario Inicial</label>
							   			 <input type="text" class="form-control" id="tinventarioinicial">
							    </div>
							     <div id="unsolopedido" style="display: none;">
							    		 <label  >Demanda Optimista</label>
							   			 <input type="text" class="form-control" id="demandaoptimista">
							   			 <label  >Demanda Pesimista</label>
							   			 <input type="text" class="form-control" id="demandapesimista">
							    </div>

							 </div>

							  <div class="form-group">
							    <label >Vida de los articulos</label>
							    <select class="form-control" id="vidadelosarticulos">
							    	<option value="-1"></option>
							    	<option value="perecederos">Perecederos</option>
							    	<option value="noperecederos">No perecederos</option>
							    </select>
							    <div id="perecederos" style="display: none;">
							    		 <label  >Periodo de caducidad del producto</label>
							   			 <input type="text" class="form-control" id="caducidaddelproducto">							   			 
							    </div>
							 </div>
							 <div class="form-group">
							    <label >Comportamiento acerca de los faltantes</label>							   
							     <select class="form-control" id="comportamientosFaltantes">
							     	<option value="-1"></option>
							    	<option value="si">Se aceptan Faltantes</option>
							    	<option value="no">No se aceptan faltantes</option>
							    </select>
							
							 </div>
							  <a  href="panel.php"   class="btn btn-default" >Salir</a>
       						<button type="button" class="btn btn-primary" id="calcular">Calcular</button>

							
					 </div>
					 <div class="col-md-6 col-xs-6 col-sm-6">
					 	<div class="input-group"  id="buscarUsuario" >
							 <input type="text" class="form-control" id="tBuscar2" placeholder="Busqueda Por Descripcion">
						      <span class="input-group-btn">
						        <button class="btn btn-primary" type="button" id="buscar2" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
						      </span>			      
						    </div>
						    <div id="alertaBusquedaporCodigo2"></div>
						    <div id="bcTarget"></div>
						     <br>
							 <hr>
							 <br>
						     <div class="form-group">
							    <label >Comportamiento acerca del tiempo de aprovisionamiento</label>							   
							     <select class="form-control" id="comportamientoAprovisionamiento">
							     	<option value="-1"></option>
							    	<option value="Deterministico">Deterministico</option>
							    	<option value="Probabilistico">Probabilistico</option>
							    </select>
								<div id="divdeterministico" style="display: none;"> 
										 <label  >Media del tiempo de aprovisionamiento</label>
							   			 <input type="text" class="form-control" id="mediatiempodeaprovsionamientoDETERMINISTICO">							   			 
								</div>
								<div id="divprobabilistico" style="display: none;"> 
										<label  >Media del tiempo de aprovisionamiento</label>
										<input type="text" class="form-control" id="mediatiempodeaprovsionamientoPROBABILISTICO">	
										<label  >Desviacion estandar del tiempo de aprovisionamiento</label>
										<input type="text" class="form-control" id="desviacionestandar">	

										<label> Unidades del tiempo de aprovisionamiento</label>
										<input type="text" class="form-control" id="tiempodeaprovisionamiento">		
										<label> Dias del año que opera el sistema</label>
										<input type="text" class="form-control" id="diasdelano">				   			 
								</div>								
							 </div>

							  <div class="form-group">
								    <label >Comportamiento del precio i</label>	
								     <select class="form-control" id="comportamientodelprecio">
								     	<option value="-1"></option>
								    	<option value="descuentosporcantidad">Descuentos por cantidad</option>
								    	<option value="preciosfijos">Precios fijos</option>
								    </select>
								    <div id="divdescuentosporcantidad" style="display: none;"> 
								    	<label>Precio 1:</label>
										<input type="text" class="form-control" id="desde1" placeholder="desde">	<input type="text" class="form-control" id="hasta1" placeholder="hasta">
										<label>Precio 2:</label>
										<input type="text" class="form-control" id="desde2" placeholder="desde">	<input type="text" class="form-control" id="hasta2" placeholder="hasta">		
								    	<label>Precio 3:</label>
										<input type="text" class="form-control" id="desde3" placeholder="desde">	<input type="text" class="form-control" id="hasta3" placeholder="hasta">	
								    	<label>Precio 4:</label>
										<input type="text" class="form-control" id="desde4" placeholder="desde">	<input type="text" class="form-control" id="hasta4" placeholder="hasta">	
								   		<label>Precio n:</label>
										<input type="text" class="form-control" id="desden" placeholder="desde">	<input type="text" class="form-control" id="hastan" placeholder="hasta">	
								    </div>
								    <div id="divpreciosfijos" style="display: none;"> 
								    	<label>Ultimo precio</label>
											<input type="text" class="form-control" id="ultimoprecio">		
								    </div>
							   </div> 
							    <div class="form-group">
								    <label >Forma en que se demandan los articulos i</label>	
								     <select class="form-control" id="formaenquesedemandanlosarticulos">
								     	<option value="-1"></option>
								    	 <option value="unoporuno">Los articulos se demandan uno por uno</option>
								    	 <option value="porlotes">Los articulos se demandan por lotes</option>
									</select>
									  <div id="divdunoporuno" style="display: none;"> 
									  		<label>Tamaño Uno a uno</label>
											<input type="text" class="form-control" id="tporuno">		
									  </div>
									  <div id="divdporlotes" style="display: none;"> 
									  		<label>Tamaño del lote(pi)</label>
											<input type="text" class="form-control" id="tporlote">	
									  </div>
								</div>
								<div class="form-group">
									 <label >Tipo de entrega de las cantidades compradas o lotes</label>
									 <select class="form-control" id="tipodeentregas">
									 	<option value="-1"></option>
								    	 <option value="pedidodeunasolavez">Pedido se entrega de una sola vez</option>
								    	 <option value="pedidoporpartes">Pedido se entrega por partes</option>
									</select>	
									<div id="divpedidodeunasolavez" style="display: none;"> 
										<label>Tamaño del pedido</label>
											<input type="text" class="form-control" id="ttamanodelpedido">
									</div>
									 <div id="divpedidoporpartes" style="display: none;"> 
									  		<label>Tamaño del lote(pi)</label>
											<input type="text" class="form-control" id="tamanoporlote">	
									  </div>
								</div>
								<div class="form-group">
									 <label >Costos involucrados en el sistema de inventario (i)</label>
									 <select class="form-control" id="costoinvolucradosenelsistemadeinventario"  multiple="multiple">
										 <option value="-1"></option>
								    	 <option value="costodemantener">Costo de mantener</option>
								    	<option value="costodecomprar">Costo de comprar</option>
								    	<option value="costodeadquirir">Costo de adquirir</option>
								    	<option value="costodefaltantes">Costo de faltantes</option>
									</select>	
									<div id="divcostopormantener" style="display: none;"> 
											<label>Costo por mantener (coi)</label>
											<input type="text" class="form-control" id="costopormantenercoi">
											<label>Costo por mantener (chfi)</label>
											<input type="text" class="form-control" id="costopormantenerchfi">
														
									</div>
									<div id="divcostoporcomprar" style="display: none;"> 
											<label>Costo por comprar(cofi)</label>
											<input type="text" class="form-control" id="costoporcomprarcofi">
											<label>Costo por comprar(coi)</label>
											<input type="text" class="form-control" id="costoporcomprarcoi">															
									</div>
									<div id="divcostoporadquirir" style="display: none;"> 
											<label>Costo por adquirir (pi)</label>
											<input type="text" class="form-control" id="costoporadquirir">														
									</div>
									<div id="divcostodepenalizacionodeescasezr" style="display: none;"> 												
											<label>Costo de penalizacion o de escasez (csi)</label>
											<input type="text" class="form-control" id="costodepenalizacionodeescasez">
									</div>

								</div>
								<div class="form-group">
									<label >Cantidad que se compra actualmente</label>
									<input type="text" class="form-control" id="qactual" placeholder="Cantidad en unidades Q actual">

								</div>


					 </div>
					
			</div>

			<div class="row">
				 <div class="col-md-12 col-xs-12 col-sm-12" id="alertasCalculos">
							
				 </div>
						
			</div>
			<!-- <div class="row">

					<div class="col-md-12 col-xs-12 col-sm-12">
								<table class="table"  id="tablaInventario2">
								<hr><br><br>
									<tr>
										<td><b>Piezas compradas</b></td>
										<td><b>Fechas</b></td>
										<td><b>Piezas vendidas</b></td>
										<td><b>Fechas</b></td>										
									</tr>
									<tr>		
										<td><input type="text" class="form-control" id="tcantidad"   ></td>								
										<td><input type="date" class="form-control" id="tfecha"   ></td>				
										
										<td><input type="text" class="form-control" id="tcosto1" ></td>
										<td><input type="date" class="form-control" id="tfecha"   ></td>	
									</tr>
								</table>
					</div>
			</div> -->
	   </div>
</div>

