<br>

<div class="panel panel-default">
 <!--  <div class="panel-heading">Menu Principal</div> -->
  		<div class="panel-body">
			<div class="container">
			    <ul class="nav nav-pills">
			       <li id="minsertar" class="active"><a href="#" id="insertar"> Insertar</a></li>
			       <li id="mconsultar"><a href="#" id="consultar"> Consultar</a></li>
			       <li id="mmodificar" style="display:none;"><a href="#" id="modificar"> Modificar</a></li>
			       <li id="meliminar"  style="display:none;"><a href="#" id="eliminar"> Eliminar</a></li>
			       <li ><a href="#" id="cerrarsesion"> Cerrar sesion</a></li>
			    </ul>
			    
			</div> 
			<hr> <br>
			<div class="row">
			    <div class="col-lg-12">			        
			        <ol class="breadcrumb">
			            <li><a href="panel.php">Inicio</a>
			            </li>
			            <li class="active">Usuarios</li>
			        </ol>
			        <h2 class="page-header">Usuarios</h2>
			    </div>
			    
			</div>
			
				<div class="row">
					<!--  <div class="col-md-1 col-xs-1 col-sm-1"></div> -->
					 <div class="col-md-5 col-xs-5 col-sm-5">
					 	<div id="labelUsuario"><h3>Agregar nuevo Usuario</h3></div>
					 	<div id="alertasUsuario"></div>
					 	<form role="form" onsubmit="return false;">
					 		<div class="input-group"  id="buscarUsuario" style="display:none;">
							 <input type="text" class="form-control" id="tBuscar" placeholder="Cedula">
						      <span class="input-group-btn">
						        <button class="btn btn-primary" type="button" id="buscar" ><span class="glyphicon glyphicon-search"></span>Buscar </button>
						      </span>			      
						    </div>
				        	<div class="form-group">
							    <label >Cedula</label>
							    <input type="text" class="form-control" id="tcedula" >
							  </div>
							  <div class="form-group">
							    <label >Nombre</label>
							  <input type="text" class="form-control" id="tnombre"  >		    
							  </div>
							  <div class="form-group">
							    <label >Correo</label>
							    <input type="text" class="form-control" id="tcorreo" >
							  </div>
							   <div class="form-group">
							    <label >Contraseña</label>
							    <input type="text" class="form-control" id="tpass" >
							  </div>
							  <div class="form-group">
							    <label >Telefono</label>
							    <input type="text" class="form-control" id="ttelefono"  >
							  </div>
							  
							   <div class="form-group">
							    <label >Direccion</label>
							   	<textarea class="form-control"  rows="4" cols="60"  id="tdireccion" ></textarea>								    
							  </div>
							  <div class="form-group">
							    <label >Tipo de Usuario</label>
							    <select class="form-control" id="tipo">    
								    <option value="0">Gerente</option>
								    <option value="1">Administrador</option>
							    </select>
							  </div>
							    
						</form>
						<a  href="panel.php"   class="btn btn-default" >Salir</a>
       					<button type="button" class="btn btn-primary" id="registrar">Aceptar</button>
					 </div>
					 <!-- <div class="col-md-6 col-xs-6 col-sm-6"></div> -->
					
			</div>


	   </div>
</div>