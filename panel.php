<?php
	session_start(); 
	if(!isset($_SESSION['nombre'])){
		header ("Location: index.html");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sistema de Inventario - Panel</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/morris.css">
	
	
		  <style type="text/css">
		   body {
					
					
					background: #fbfbfb url(img/bg1.jpg) 0 0 repeat;
					
				}

			</style>
</head>
<body>
	<div class="container" >
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		 
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Sistema de Inventario</a>
		    </div>

		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <!-- <li class="active"id="inventario2"><a href="#" id="inventario">Gestionar Inventario</a></li>		       
		        <li id="datos2"><a href="#" id="datos">Gestionar Datos</a></li>
		         <li id="usuarios2"><a href="#" id="usuarios">Gestionar Usuario</a></li> -->
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		      	<li><a > <?php  echo $_SESSION['nombre']; ?> </a></li>
		        <li><a href="#" id="cerrarsesion">Cerrar Sesion</a></li>
		       </ul>

		    </div>
		  </div>
		</nav>
		</div> 
		
		<div class="container" id="contenedor">
			 
		</div>
		<center>
			
		
		<div class="container" >
			<div class="panel panel-default">
			  <div class="panel-body">
			    Todos los derechos reservados
			  </div>
			</div> 
		</div>
		</center>



		<script type="text/javascript" src="include/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/eventosPartials.js"></script>
		<script type="text/javascript">
		eventos();
		</script>
		<!--   <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script type="text/javascript" src="include/morris.min.js"></script> -->
		<script type="text/javascript" src="include/jquery-barcode-last.min.js"></script>
</body>
</html>