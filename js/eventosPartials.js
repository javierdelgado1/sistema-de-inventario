function eventos(){
	$("#contenedor").hide().load('partials/index.html', function(){ inicio(); }).fadeIn(1000);
}

function inicio(){
	opcion1.onclick=function(){
	$("#contenedor").hide().load('partials/articulosyproveedores.php', function(){ articulosyproveedores(); }).fadeIn(1000);
	}
	opcion2.onclick=function(){
	$("#contenedor").hide().load('partials/usuario2.php', function(){ usuarios(); }).fadeIn(1000);
	}
	opcion3.onclick=function(){
		$("#contenedor").hide().load('partials/inventario2.php', function(){ eventosInventario(); }).fadeIn(1000);
	}
	opcion4.onclick=function(){
		$("#contenedor").hide().load('partials/utilidades.html', function(){ /*eventosInventario();*/ }).fadeIn(1000);
	}

	cerrarsesion.onclick=function(){
		CerrarSesion();
	}


}

function eventosInventario(){
	buscar.onclick=function(){
		if(tBuscar.value!=""){
			$.ajax
			({
				type: "POST",
				url: "modelo/consultasInventario.php",
				data: {id:3, tipo:1, data:tBuscar.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
						if(msg[0].m>0)
					{
						tcodigo.value=msg[0].codigo;
						tdescripcion.value=msg[0].descripcion;
						tpreciodelista.value=msg[0].costo;
						alertaBusquedaporCodigo.innerHTML="";
						 $("#bcTarget").barcode("123", "ean13"); 						
						
					}
					else{
						alertaBusquedaporCodigo.innerHTML="<div class='alert alert-warning' role='alert'>¡Producto no encontrado!</div>";
					}					
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
	}
	buscar2.onclick=function(){
		if(tBuscar2.value!=""){
			$.ajax
			({
				type: "POST",
				url: "modelo/consultasInventario.php",
				data: {id:3, tipo:0, data:tBuscar2.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
						if(msg[0].m>0)
					{
						tcodigo.value=msg[0].codigo;
						tdescripcion.value=msg[0].descripcion;
						tpreciodelista.value=msg[0].costo;
						alertaBusquedaporCodigo2.innerHTML="";
						 $("#bcTarget").barcode("123", "ean13"); 
												
						
					}
					else{
						alertaBusquedaporCodigo2.innerHTML="<div class='alert alert-warning' role='alert'>¡Producto no encontrado!</div>";
					}					
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
	}

	calcular.onclick=function(){
		/*$.ajax
			({
				type: "POST",
				url: "modelo/calculo.php",
				data: {
						numerodeestaciones:numerodeestaciones.value,
						demandaporestaciones:demandaporestaciones.value,
						tTipodemanda:tTipodemanda.value,
						tiempodeciclo:tiempodeciclo.value,
						tinventarioinicial:tinventarioinicial.value,
						demandaoptimista:demandaoptimista.value,
						demandapesimista:demandapesimista.value,
						caducidaddelproducto:caducidaddelproducto.value,
						mediatiempodeaprovsionamientoDETERMINISTICO:mediatiempodeaprovsionamientoDETERMINISTICO.value,
						mediatiempodeaprovsionamientoPROBABILISTICO:mediatiempodeaprovsionamientoPROBABILISTICO.value,
						desviacionestandar:desviacionestandar.value,
						tiempodeaprovisionamiento:tiempodeaprovisionamiento.value,
						diasdelano:diasdelano.value,
						desde1:desde1.value,
						desde2:desde2.value,
						desde3:desde3.value,
						desde4:desde4.value,
						desden:desden.value,
						hasta1:hasta1.value,
						hasta2:hasta2.value,
						hasta3:hasta3.value,
						hasta4:hasta4.value,
						hastan:hastan.value,
						ultimoprecio:ultimoprecio.value,
						tporuno:tporuno.value,
						tporlote:tporlote.value,
						ttamanodelpedido:ttamanodelpedido.value,
						tamanoporlote:tamanoporlote.value,
						costopormantenercoi:costopormantenercoi.value,
						costopormantenerchfi:costopormantenerchfi.value,
						costoporcomprarcofi:costoporcomprarcofi.value,
						costoporcomprarcoi:costoporcomprarcoi.value,
						costoporadquirir:costoporadquirir.value,
						costodepenalizacionodeescasez:costodepenalizacionodeescasez.value,
						qactual:qactual.value
				},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
					console.log(msg);				
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});*/

			console.log("Restricciones de espacio en el almacenamiento: " + restriccionesdeespacio.value +
						" Tipo de Demanda: "+ tipodedemanda.value +
						" Tipo de sistema de control de inventario seleccionado: " + tipodesistemadecontrolseleccionado.value +
						" Vida de los articulos: " +	vidadelosarticulos.value + 
						" Comportamiento acerca de los faltantes: " +comportamientosFaltantes.value +
						" Comportamiento acerca del tiempo de aprovisionamiento: " + comportamientoAprovisionamiento.value +
						" Comportamiento del precio i: " + comportamientodelprecio.value +
						" Forma en que se demandan los articulos i: " +	formaenquesedemandanlosarticulos.value +
						" Tipo de entrega de las cantidades compradas o lotes: " + tipodeentregas.value+
						" Costos involucrados en el sistema de inventario (i): " + $('#costoinvolucradosenelsistemadeinventario').val());
	
			
			var selectedOptions = $('#costoinvolucradosenelsistemadeinventario option:selected');
			console.log("Tamano de arreglo: " +selectedOptions.length);

	if(selectedOptions.length==2){
		if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||	restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||	restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&	comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&	comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"
			){
				console.log("4.3.1 Modelo EOQ Deterministico");
			}
	}
	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){

			console.log("4.3.1 Modelo EOQ Deterministico");
	}

	if(selectedOptions.length==2){		
		if(	restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&	formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&	formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"){
				console.log("4.3.2 Periodo Fijo Deterministico");
		}
	}

	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&	formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&	tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){

			console.log("4.3.2 Periodo Fijo Deterministico");
	}

	if(selectedOptions.length==2){	
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"

			){
				console.log("4.3.3 Modelo eoq de entrega gradual");

		}
	}
	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"

			){
				console.log("4.3.3 Modelo eoq de entrega gradual");
	}
	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"
			){
			console.log("4.3.4 EOQ  con descuentos");
		}
	}
	else if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){

			console.log("4.3.4 EOQ  con descuentos");
	}

	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value==""&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value==""&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||



			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"){
			console.log("4.3.5 EOQ Se permiten faltantes");
		}
	}

	else if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="si"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){

			console.log("4.3.5 EOQ Se permiten faltantes");
	}

	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||





			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){
			console.log("4.3.6 EOQ  para productos perecederos");
		}
	}

	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Crecienteodecreciente"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="horizontal"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
			
			console.log("4.3.6 EOQ  para productos perecederos");
	}
	if(selectedOptions.length==2){	
		if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"
			){
			console.log("4.3.7 Modelo EOQ-Probabilistico");
		}
	}
	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"

			){
			console.log("4.3.7 Modelo EOQ-Probabilistico");
	}

	if(selectedOptions.length==2){
		if(
			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||



			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){
			console.log("4.3.8 EOQ-Probabilistico  de entrega gradual");

		}
	}

	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidoporpartes"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
			
			console.log("4.3.8 EOQ-Probabilistico  de entrega gradual");
	}
	if(selectedOptions.length==2){
		if(	restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&	comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||


			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&	comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"
			){
			console.log("4.3.9 EOQ-Probabilistico  con descuentos");
		}
	}

	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener" ||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&	comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){
		console.log("4.3.9 EOQ-Probabilistico  con descuentos");
	}

	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"

			){
			console.log("4.3.10 EOQ-Probabilistico  con  tiempo de entrega Probabilistica y descuento por cantidad");
		}

	}

	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Probabilistico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
			console.log("4.3.10 EOQ-Probabilistico  con  tiempo de entrega Probabilistica y descuento por cantidad");

	}		//Aclarar dudo con puntual y probabilistico

	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="Unsolopedido"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodefaltantes"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="Unsolopedido"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"

			){
			console.log("4.3.11 Un solo pedido Probabilistico");
		}
	}
	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="Unsolopedido"&&
			vidadelosarticulos.value=="perecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodefaltantes"){
			console.log("4.3.11 Un solo pedido Probabilistico");
	}
	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){
			console.log("4.3.12 Periodo fijo Probabilistico");
		}
	}
	else if(restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="no"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="periodofijo"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="descuentosporcantidad"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
			console.log("4.3.12 Periodo fijo Probabilistico");
	}
	if(selectedOptions.length==2){
		if( restriccionesdeespacio.value=="no"&&
			tipodedemanda.value=="Deterministica"&&
			tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&
			comportamientosFaltantes.value=="no"&&
			comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&
			formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidoporpartes"&&			
			costoinvolucradosenelsistemadeinventario.value=="costodecomprar"
			){
			console.log("4.3.13 Modelo deterministico de productos multiples y entrega gradual");
		}
	}
	if(selectedOptions.length==1){
		if(restriccionesdeespacio.value=="si"&&
			tipodedemanda.value=="horizontal"&&
			tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&
			comportamientosFaltantes.value=="no"&&
			comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&
			formaenquesedemandanlosarticulos.value=="unoporuno"&&
			tipodeentregas.value=="pedidodeunasolavez"&&			
			costoinvolucradosenelsistemadeinventario.value=="costodecomprar"

			){
			console.log("4.3.14 Modelo EOQ Deterministico con Restricciones de almacenamiento");
		}
	}
	if(selectedOptions.length==2){
		if(restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"||

			restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"||

			restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"
			){
			console.log("4.3.15 Modelo EOQ probabilistico con restricciones de almacenamiento");
		}
	}
	else if(restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"||

			restriccionesdeespacio.value=="si"&&tipodedemanda.value=="Probabilistica"&&tipodesistemadecontrolseleccionado.value=="cantidadFija"&&
			vidadelosarticulos.value=="noperecederos"&&comportamientosFaltantes.value=="no"&&comportamientoAprovisionamiento.value=="Deterministico"&&
			comportamientodelprecio.value=="preciosfijos"&&formaenquesedemandanlosarticulos.value=="unoporuno"&&tipodeentregas.value=="pedidodeunasolavez"&&			
			$('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){

			console.log("4.3.15 Modelo EOQ probabilistico con restricciones de almacenamiento");
	}
}
	restriccionesdeespacio.onchange = function () 
	{
		/*if(restriccionesdeespacio.value=="si"){
			$('#Restriccionesdeespacio2').fadeIn(1500);
		} 
		if(restriccionesdeespacio.value=="no"){
			$('#Restriccionesdeespacio2').fadeOut(1500);
		}*/
	}
	tipodedemanda.onchange = function () 
	{
		/*if(tipodedemanda.value=="horizontal"){
			$('#tipodedemanda2').fadeOut(1500);
			$('#tipodedemanda3').fadeIn(1500);			
			$('#ltipodemanda').html("Media de la demanda proyectada");
		} 
		if(tipodedemanda.value=="Crecienteodecreciente"){
			$('#tipodedemanda2').fadeOut(1500);
			$('#tipodedemanda3').fadeIn(1500);			
			$('#ltipodemanda').html("Demanda proyectada para el periodo");
		}
		if(tipodedemanda.value=="dependiente"){
			$('#tipodedemanda2').fadeOut(1500);
			$('#tipodedemanda3').fadeIn(1500);
			
			$('#ltipodemanda').html("Demanda proyectada para el periodo");
		}
		if(tipodedemanda.value=="estacional"){
			$('#tipodedemanda3').fadeOut(1000);
			$('#tipodedemanda2').fadeIn(1500);
			
			//ltipodemanda.innerHTML="Demanda proyectada para el periodo";
		}*/
	}

	tipodesistemadecontrolseleccionado.onchange=function () {
		/*if (tipodesistemadecontrolseleccionado.value=="periodofijo") {
			$('#unsolopedido').fadeOut(1000);
			$('#divperiodofijo').fadeIn(1500);
		}
		if (tipodesistemadecontrolseleccionado.value=="Unsolopedido") {
			$('#divperiodofijo').fadeOut(1000);
			$('#unsolopedido').fadeIn(1500);
		}*/
	}
	vidadelosarticulos.onchange=function () {
		/*if(vidadelosarticulos.value=="perecederos"){
			$('#perecederos').fadeIn(1500);
		}
		if(vidadelosarticulos.value=="perecederos"){
			$('#perecederos').fadeOut(1500);
		}*/

	}
	comportamientosFaltantes.onchange=function(){
		/*if(comportamientosFaltantes.value=="si"){
			//$('#perecederos').fadeIn(1500);
		}
		if(comportamientosFaltantes.value=="no"){
			//$('#perecederos').fadeIn(1500);
		}*/
	}
	comportamientoAprovisionamiento.onchange=function(){
		/*if(comportamientoAprovisionamiento.value=="si"){
			$('#divprobabilistico').fadeOut();
			$('#divdeterministico').fadeIn(1500);
		}
		if(comportamientoAprovisionamiento.value=="no"){
			$('#divdeterministico').fadeOut();
			$('#divprobabilistico').fadeIn(1500);
		}*/
	}
	comportamientodelprecio.onchange=function(){
		/*if(comportamientodelprecio.value=="descuentosporcantidad"){
			$('#divpreciosfijos').fadeOut();
			$('#divdescuentosporcantidad').fadeIn(1500);
		}
		if(comportamientodelprecio.value=="preciosfijos"){
			$('#divdescuentosporcantidad').fadeOut();
			$('#divpreciosfijos').fadeIn(1500);
		}*/
	}

	formaenquesedemandanlosarticulos.onchange=function(){
		/*if(formaenquesedemandanlosarticulos.value=="unoporuno"){
			$('#divdporlotes').fadeOut();
			$('#divdunoporuno').fadeIn(1500);
		}
		if(formaenquesedemandanlosarticulos.value=="porlotes"){
			$('#divdunoporuno').fadeOut();
			$('#divdporlotes').fadeIn(1500);
		}*/
	}
	tipodeentregas.onchange=function(){
		/*if(tipodeentregas.value=="pedidodeunasolavez"){
			$('#divpedidoporpartes').fadeOut();
			$('#divpedidodeunasolavez').fadeIn(1500);
		}
		if(tipodeentregas.value=="pedidoporpartes"){
			$('#divpedidodeunasolavez').fadeOut();
			$('#divpedidoporpartes').fadeIn(1500);
		}*/
	}

	$('#costoinvolucradosenelsistemadeinventario').multiselect({
        onChange: function(option, checked) {
            // Get selected options.
            var selectedOptions = $('#costoinvolucradosenelsistemadeinventario option:selected');
 
 			/*if (selectedOptions.length ==1) {
               
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostopormantener').fadeIn(1500);
                }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeIn(1500);
                	
                }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodeadquirir"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);                	
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostoporadquirir').fadeIn(1500);
                }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodefaltantes"){
                	$('#divcostodepenalizacionodeescasezr').fadeIn(1500);                	
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                }

            }*/

            if (selectedOptions.length >=2) {
                // Disable all other checkboxes.
                var nonSelectedOptions = $('#costoinvolucradosenelsistemadeinventario option').filter(function() {
                    return !$(this).is(':selected');
                });
 
                var dropdown = $('#costoinvolucradosenelsistemadeinventario').siblings('.multiselect-container');
                nonSelectedOptions.each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', true);
                    input.parent('li').addClass('disabled');
                });
               /*  if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodemantener"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostopormantener').fadeIn(1500);

		                if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"){		                	
		                	$('#divcostoporcomprar').fadeIn(1000);               
		                }
		                 if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodeadquirir"){		                	
		                	$('#divcostoporadquirir').fadeIn(1500);            
		                }
		                if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodefaltantes"){		                	
		                	$('#divcostodepenalizacionodeescasezr').fadeIn(1500);            
		                }


           		 }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodecomprar"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeIn(1500);
                	 	if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){		                	
		                	$('#divcostopormantener').fadeIn(1500);              
		                }
		                 if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodeadquirir"){		                	
		                	$('#divcostoporadquirir').fadeIn(1500);            
		                }
		                if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodefaltantes"){		                	
		                	$('#divcostodepenalizacionodeescasezr').fadeIn(1500);            
		                }
                }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodeadquirir"){
                	$('#divcostodepenalizacionodeescasezr').fadeOut(1000);                	
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostoporadquirir').fadeIn(1500);

                		if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){		                	
		                	$('#divcostopormantener').fadeIn(1500);              
		                }
		                 if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"){		                	
		                	$('#divcostoporcomprar').fadeIn(1500);        
		                }
		                if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodefaltantes"){		                	
		                	$('#divcostodepenalizacionodeescasezr').fadeIn(1500);            
		                }

                }
                if($('#costoinvolucradosenelsistemadeinventario').val()[0]=="costodefaltantes"){
                	$('#divcostodepenalizacionodeescasezr').fadeIn(1500);                	
                	$('#divcostopormantener').fadeOut(1000);
                	$('#divcostoporcomprar').fadeOut(1000);
                	$('#divcostoporadquirir').fadeOut(1000);
                		if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodemantener"){		                	
		                	$('#divcostopormantener').fadeIn(1500);              
		                }
		                 if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodecomprar"){		                	
		                	$('#divcostoporcomprar').fadeIn(1500);        
		                }
		                if($('#costoinvolucradosenelsistemadeinventario').val()[1]=="costodeadquirir"){		                	
		                	$('#divcostoporadquirir').fadeIn(1500);            
		                }
                }*/



            }
            else {
                // Enable all checkboxes.
                var dropdown = $('#costoinvolucradosenelsistemadeinventario').siblings('.multiselect-container');
                $('#costoinvolucradosenelsistemadeinventario option').each(function() {
                    var input = $('input[value="' + $(this).val() + '"]');
                    input.prop('disabled', false);
                    input.parent('li').addClass('disabled');
                });
            }
        }
    });

}
function usuarios(){
	$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:9},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					console.log("Privilegio " + msg);
					if(msg==1)	{
						$('#mmodificar').fadeIn();
						$('#meliminar').fadeIn();
					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
	var estadoU=1;
	mconsultar.onclick=function(){
		$('#buscarUsuario').fadeIn();
		$('#mconsultar').addClass("active");
		$('#minsertar').removeClass("active");
		$('#meliminar').removeClass("active");
		$('#mmodificar').removeClass("active");
		$('#labelUsuario').html("<h1>Consultar Usuario</h1>");
		$('#alertasUsuario').html("");
		$('#registrar').fadeOut();
		$("#tcedula").prop('disabled', false);
		$("#tcorreo").prop('disabled', false);
		tBuscar.value="";
	}
	meliminar.onclick=function(){
		$('#registrar').fadeIn();
		$('#buscarUsuario').fadeIn();
		$('#meliminar').addClass("active");
		$('#minsertar').removeClass("active");
		$('#mconsultar').removeClass("active");
		$('#mmodificar').removeClass("active");
		$('#labelUsuario').html("<h1>Eliminar Usuario</h1>");
		$('#registrar').html('<span class="glyphicon glyphicon-trash"> Eliminar</span>');
		$('#alertasUsuario').html("");
		$("#tcedula").prop('disabled', false);
		$("#tcorreo").prop('disabled', false);
		tBuscar.value="";
		estadoU=3;
	}
	mmodificar.onclick=function(){
		$('#registrar').fadeIn();
		$('#buscarUsuario').fadeIn();
		$('#mmodificar').addClass("active");
		$('#mconsultar').removeClass("active");
		$('#meliminar').removeClass("active");
		$('#minsertar').removeClass("active");
		$('#labelUsuario').html("<h1>Modificar Articulo</h1>");
		$('#registrar').html('<span class="glyphicon glyphicon-refresh">Modificar</span>');		
		$('#alertasUsuario').html("");
		$("#tcedula").prop('disabled', true);
		$("#tcorreo").prop('disabled', true);
		tBuscar.value="";
		estadoU=2;
	}
	minsertar.onclick=function(){
				$('#registrar').fadeIn();
				$('#buscarUsuario').fadeOut();
				$('#minsertar').addClass("active");
				$('#mconsultar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelUsuario').html("<h1>Insertar Nuevo Articulo</h1>");
				$('#registrar').html('Registrar');	
				//$('#labelBuscar_Consultar').html("Consultar");
				$('#alertasUsuario').html("");

				$("#tcedula").prop('disabled', false);
				$("#tcorreo").prop('disabled', false);
				estadoU=1;
	}
	registrar.onclick=function(){
		if(estadoU==1)
			registrarUsuario();
		if(estadoU==2)
			modificarUsuario();
		if(estadoU==3)
			eliminarUsuario();
	}
	cerrarsesion.onclick=function(){
		CerrarSesion();
	}
	buscar.onclick=function(){
		 buscarUsuario();
	}
}

function limpiarformusuario(){
	tcedula.value="";
	tnombre.value="";
	tcorreo.value="";
	tpass.value="";
	ttelefono.value="";
	tdireccion.value="";
}
function eliminarUsuario(){
	$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:6, cedula:tcedula.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){				
						alertasUsuario.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha eliminado Correctamente!</div>";
						limpiarformusuario();
					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
}
function modificarUsuario(){
	if(validarRegistroUsuario()){	
			$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:5, cedula:tcedula.value, nombre:tnombre.value,  correo:tcorreo.value,  pass:tpass.value, telefono:ttelefono.value, direccion:tdireccion.value, tipo:tipo.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){
						
						alertasUsuario.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha actualizado Correctamente!</div>";
						limpiarformusuario();
						


					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}
function buscarUsuario(){
	if(tBuscar.value!=""){
		$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:3, cedula:tBuscar.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
						if(msg[0].m>0)
					{
						tcedula.value=msg[0].cedula;
						tnombre.value=msg[0].nombre;
						tcorreo.value=msg[0].correo;
						tpass.value=msg[0].pass;
						ttelefono.value=msg[0].telefono;
						tdireccion.value=msg[0].direccion;						
						$('#tipo').val(msg[0].tipo);
					}
					else{
						alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Usuario no encontrado!</div>";
					}					
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
	}
}
function registrarUsuario(){
	if(validarRegistroUsuario()){
	
			$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:1, cedula:tcedula.value, nombre:tnombre.value,  correo:tcorreo.value,  pass:tpass.value, telefono:ttelefono.value, direccion:tdireccion.value, tipo:tipo.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="false"){
						alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡El correo o la cedula ya se encuentra registrado, elija otro!</div>";
						
					}
					else{
						alertasUsuario.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha Registrado Correctamente!</div>";
						limpiarformusuario();

					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}
function articulosyproveedores(){
	//cerrarsesion();
		$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:9},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					console.log("Privilegio " + msg);
					if(msg==1)	{
						$('#mmodificar').fadeIn();
						$('#meliminar').fadeIn();
					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
	var ban=0;
	var estadoP=1, estadoA=1;
	llenarSelectProveedor();
	opcArticulo.onclick=function(){
		$('#labelnombre').html("<h1 class='page-header' >Articulo</h1>");
		$('#articulo').fadeIn(1000);
		$('#proveedor').hide();
		$('#opcArticulo').addClass("active");
		$('#opcProveedor').removeClass("active");
		ban=0;
		$('#buscarArticulo').hide();
		$('#labelArticulo').html("<h1>Insertar Nuevo Articulo</h1>");
		$('#minsertar').addClass("active");
		$('#mconsultar').removeClass("active");
		$('#mmodificar').removeClass("active");
		$('#meliminar').removeClass("active");	
		llenarSelectProveedor();
		estadoP=-1;	
		$('#tablaArticulo').fadeIn();
		
	}
	opcProveedor.onclick=function(){
		$('#labelnombre').html("<h1 class='page-header' >Proveedor</h1>");
		$('#articulo').hide();
		$('#proveedor').fadeIn(1000);
		$('#opcProveedor').addClass("active");
		$('#opcArticulo').removeClass("active");
		$('#buscarProveedor').hide();
		ban=1;

		$('#labelProveedor').html("<h1>Insertar Nuevo Proveedor</h1>");

		$('#minsertar').addClass("active");
		$('#mconsultar').removeClass("active");
		$('#mmodificar').removeClass("active");
		$('#meliminar').removeClass("active");		
		estadoA=-1;
		$('#tablaArticulo').fadeOut();
		
	}
	eliminar.onclick=function(){

		$('#aceptar').fadeIn();
	 	$('#aceptar2').fadeIn();
			if(ban==0){
				$('#buscarArticulo').fadeIn();
				$('#meliminar').addClass("active");
				$('#minsertar').removeClass("active");
				$('#mconsultar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelArticulo').html("<h1>Eliminar Articulo</h1>");
				$('#labelBuscar_Consultar').html("Buscar Articulo");
				$('#aceptar2').html('<span class="glyphicon glyphicon-trash"> Eliminar</span>');
				estadoA=3;
				$('#alertasArticulo').html("");
				$("#tcodigo").prop('disabled', false);

			}
			else{
				$('#buscarProveedor').fadeIn();
				$('#meliminar').addClass("active");
				$('#minsertar').removeClass("active");
				$('#mconsultar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelProveedor').html("<h1>Eliminar Proveedor</h1>");
				$('#labelBuscar_Consultar2').html("Buscar Proveedor");
				$('#aceptar').html('<span class="glyphicon glyphicon-trash"> Eliminar</span>');
				$('#alertasProveedor').html("");
				estadoP=3;
				$("#tcodigo2").prop('disabled', false);
			}			
		}
	 consultar.onclick=function(){
	 	$('#aceptar').hide();
	 	$('#aceptar2').hide();
	 	if(ban==0){
				$('#buscarArticulo').fadeIn();
				$('#mconsultar').addClass("active");
				$('#minsertar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelArticulo').html("<h1>Consultar Articulo</h1>");
				$('#labelBuscar_Consultar').html("Consultar");
				$('#alertasArticulo').html("");
				$("#tcodigo").prop('disabled', false);
			}
			else{
				$('#buscarProveedor').fadeIn();
				$('#mconsultar').addClass("active");
				$('#minsertar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelProveedor').html("<h1>Consultar Proveedor</h1>");
				$('#labelBuscar_Consultar2').html("Consultar");
				$('#alertasProveedor').html("");
				$("#tcodigo2").prop('disabled', false);
			}
	 }
 	 insertar.onclick=function(){
 	 	
 	 	$('#aceptar').fadeIn();
	 	$('#aceptar2').fadeIn();
	 	if(ban==0){
				$('#buscarArticulo').hide();
				$('#minsertar').addClass("active");
				$('#mconsultar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelArticulo').html("<h1>Insertar Nuevo Articulo</h1>");
				//$('#labelBuscar_Consultar').html("Consultar");
				$('#aceptar2').html('Registrar');
				llenarSelectProveedor();
				estadoA=1;
				$('#alertasArticulo').html("");
				$("#tcodigo").prop('disabled', false);
			}
			else{
				$('#buscarProveedor').hide();
				$('#minsertar').addClass("active");
				$('#mconsultar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#mmodificar').removeClass("active");
				$('#labelProveedor').html("<h1>Insertar Nuevo Proveedor</h1>");
				//$('#labelBuscar_Consultar2').html("Consultar");
				$('#aceptar').html('Registrar');
				$('#alertasProveedor').html("");
				estadoP=1;
				$("#tcodigo2").prop('disabled', false);
			}
	 }

	 modificar.onclick=function(){
	 	$('#aceptar').fadeIn();
	 	$('#aceptar2').fadeIn();
	 	if(ban==0){
				$('#buscarArticulo').fadeIn();
				$('#mmodificar').addClass("active");
				$('#mconsultar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#minsertar').removeClass("active");
				$('#labelArticulo').html("<h1>Modificar Articulo</h1>");
				$('#aceptar2').html('<span class="glyphicon glyphicon-refresh">Modificar</span>');
				//$('#labelBuscar_Consultar').html("Consultar");
				estadoA=2;
				$('#alertasArticulo').html("");
				$("#tcodigo").prop('disabled', true);
			}
			else{
				$('#buscarProveedor').fadeIn();
				$('#mmodificar').addClass("active");
				$('#mconsultar').removeClass("active");
				$('#meliminar').removeClass("active");
				$('#minsertar').removeClass("active");
				$('#labelProveedor').html("<h1>Modificar Proveedor</h1>");
				$('#aceptar').html('<span class="glyphicon glyphicon-refresh">Modificar</span>');
				$('#alertasProveedor').html("");
				$("#tcodigo2").prop('disabled', true);
				estadoP=2;
				//$('#labelBuscar_Consultar2').html("Consultar");
			}
	 }
	 aceptar.onclick=function(){
	 	if(estadoP==1){
	 		RegistrarProveedor();	
	 	}
		if(estadoP==2){
	 		ModificarProveedor();	
	 	}
	 	if(estadoP==3){
	 		eliminarProveedor();	
	 	}

	 	
	 }
	 aceptar2.onclick=function(){
	 	if(estadoA==1){
	 			console.log("click en registrar articulo");
	 			registrarArticulo();
	 	}
		if(estadoA==2){
	 		ModificarArticulo();
	 	}
	 	if(estadoA==3){
			eliminarArticulo();
	 	}
	 }
	buscar2.onclick=function(){
		buscarProveedor();
	}
	buscar.onclick=function(){
		buscarArticulo();
	}

	cerrarsesion.onclick=function(){
		CerrarSesion();
	}
}

function CerrarSesion(){
	$.ajax
			({
				type: "POST",
				url: "modelo/consultasusuario.php",
				data: {id:8},
				async: false,				
				success:
				function (msg) 
				{			
					window.open('index.html' , '_self');
				},
			error:
			function (msg) {
				console.log( msg +"No se pudo realizar la consulta para iniciar sesion");}
			});
}
function ModificarArticulo(){
	if(validarRegistroArticulo()){
			
			$.ajax
			({
				type: "POST",
				url: "modelo/Articulos.php",
				data: {id:3, codigo:tcodigo.value, descripcion:tdescripcion.value, serial:tserial.value,  proveedor:tproveedor.value, ubicacion:tUbicacion2.value, area:tarea.value, ordecompra:tordencompra.value, fecha1:tfecha.value, fecha2:tfecha2.value, cantidad:tcantidad.value, costo:tcosto.value, comentario:tcomentario.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){				
						alertasProveedor.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha Modificado Correctamente!</div>";
						limpiarFormArticulos();

					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}

function limpiarFormArticulos(){
		tcodigo.value="";
		tdescripcion.value="";
		tresponsable.value="";
		tUbicacion2.value="";
		tarea.value="";			
		tserial.value="";
		tordencompra.value="";
		tfecha.value="";
		tfecha2.value="";
		tcantidad.value="";
		tcosto.value="";
		tcomentario.value="";
}
function registrarArticulo(){
	if(validarRegistroArticulo()){
			console.log("PROVEEDOR " + tproveedor.value);
			$.ajax
			({
				type: "POST",
				url: "modelo/Articulos.php",
				data: {id:1, codigo:tcodigo.value, descripcion:tdescripcion.value, serial:tserial.value,  proveedor:tproveedor.value, ubicacion:tUbicacion2.value, area:tarea.value, ordecompra:tordencompra.value, fecha1:tfecha.value, fecha2:tfecha2.value, cantidad:tcantidad.value, costo:tcosto.value, comentario:tcomentario.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="false"){
						alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡El codigo se encuentra registrado, elija otro!</div>";
					}
					else{
						alertasArticulo.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha Registrado Correctamente!</div>";
						limpiarFormArticulos();			

					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}

function llenarSelectProveedor(){
	$.ajax
			({
				type: "POST",
				url: "modelo/Proveedor.php",
				data: {id:5},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					$('#tproveedor').empty();
					for(i=0; i<msg[0].m-1; i++)
						{			
							
							tproveedor.options[i]= new Option (msg[i].codigo);
							tproveedor.options[i].text = msg[i].codigo+"";
							tproveedor.options[i].value = msg[i].id;	
						}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
}
function eliminarArticulo(){
		$.ajax
			({
				type: "POST",
				url: "modelo/Articulos.php",
				data: {id:4, codigo:tcodigo.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){				
						alertasArticulo.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha eliminado Correctamente!</div>";
						limpiarFormArticulos();
					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
}
function eliminarProveedor(){
	$.ajax
			({
				type: "POST",
				url: "modelo/Proveedor.php",
				data: {id:4, codigo:tcodigo2.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){				
						alertasProveedor.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha eliminado Correctamente!</div>";
						tcodigo2.value="";
						tdescripcion2.value="";
						tresponsable.value="";
						tUbicacion.value="";
						ttelefono.value="";
						tRif.value="";
					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
}
function ModificarProveedor(){
	if(validarRegistroProveedor()){
			console.log("entro");
			$.ajax
			({
				type: "POST",
				url: "modelo/Proveedor.php",
				data: {id:3, codigo:tcodigo2.value, descripcion:tdescripcion2.value, responsable:tresponsable.value, ubicacion:tUbicacion.value, telefono:ttelefono.value, rif:tRif.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="true"){				
						alertasProveedor.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha Modificado Correctamente!</div>";
						tcodigo2.value="";
						tdescripcion2.value="";
						tresponsable.value="";
						tUbicacion.value="";
						ttelefono.value="";
						tRif.value="";

					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}
function buscarProveedor(){
	if(tBuscar2.value!=""){
		$.ajax
			({
				type: "POST",
				url: "modelo/Proveedor.php",
				data: {id:2, codigo:tBuscar2.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
						if(msg[0].m>0)
					{
						tcodigo2.value=msg[0].codigo;
						tdescripcion2.value=msg[0].descripcion;
						tresponsable.value=msg[0].responsable;
						tUbicacion.value=msg[0].ubicacion;
						ttelefono.value=msg[0].telefono;
						tRif.value=msg[0].rif;
					}
					else{
						alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Codigo no encontrado!</div>";
					}

					
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
	}
}
function buscarArticulo(){
	if(tBuscar.value!=""){
		$.ajax
			({
				type: "POST",
				url: "modelo/Articulos.php",
				data: {id:2, codigo:tBuscar.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					
						if(msg[0].m>0)
					{
						tcodigo.value=msg[0].codigo;
						tdescripcion.value=msg[0].descripcion;
						tresponsable.value=msg[0].responsable;
						tUbicacion2.value=msg[0].ubicacion;
						tarea.value=msg[0].area		
						tserial.value=msg[0].serial;

						tordencompra.value=msg[0].ordendecompra;
						tfecha.value=msg[0].fecha1;
						tfecha2.value=msg[0].fecha2;
						tcantidad.value=msg[0].cantidad;
						tcosto.value=msg[0].costo;
						tcomentario.value=msg[0].comentario;

						llenarSelectProveedor();
						$('#tproveedor').val(msg[0].proveedor);
					}
					else{
						alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Codigo no encontrado!</div>";
					}					
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
	}
}
function RegistrarProveedor(){
	if(validarRegistroProveedor()){
			console.log("entro");
			$.ajax
			({
				type: "POST",
				url: "modelo/Proveedor.php",
				data: {id:1, codigo:tcodigo2.value, descripcion:tdescripcion2.value, responsable:tresponsable.value, ubicacion:tUbicacion.value, telefono:ttelefono.value, rif:tRif.value},
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					if(msg=="false"){
						alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡El codigo se encuentra registrado, elija otro!</div>";
					}
					else{
						alertasProveedor.innerHTML="<div class='alert alert-success' role='alert'>¡Se ha Registrado Correctamente!</div>";
						tcodigo2.value="";
						tdescripcion2.value="";
						tresponsable.value="";
						tUbicacion.value="";
						ttelefono.value="";
						tRif.value="";

					}
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
		}
}
function BuscarProducto(){
	buscarProducto.onclick=function(){
		if(tBuscarProducto.value!=""){ 
		$.ajax
			({
				type: "POST",
				url: "modelo/consultasInventario.php",
				data: {id:3, tBuscarProducto:tBuscarProducto.value },
				async: false,
				dataType: "json",
				success:
				function (msg)
				{
					var table = $('<table></table>');
					var row=$('<tr></tr>');
					ListaProductos.innerHTML="";
					if(msg[0].m>0)
						$('#ListaProductos').append(ListarTabladeProductos(msg, table, row));
					else{
						ListaProductos.innerHTML='<div class="alert alert-warning fade in" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>¡Producto No encontrado,  <a href="#" data-toggle="modal" data-target="#ModalRegistrarProducto" >  Registrar Producto</a>!</div>';
						//setTimeout(5000);
						//$('#ModalRegistrarProducto').modal('show')
					}
				
				},
			error:
			function (msg) {
				console.log("No se pudo realizar la conexion");}
			});
			EditarProducto(); EliminarProducto();
		}
	}
}




	function validarRegistroArticulo(){
		if(tcodigo.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo codigo!</div>";
			return false;
		}		

		
		else if(tdescripcion.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo descripcion!</div>";
			return false;
		}
		
		else if(tserial.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo serial!</div>";
			return false;
		}
		/*else if(tproveedor.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo T!</div>";
			return false;
		}	*/
		else if(tUbicacion2.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Ubicacion!</div>";
			return false;
		}
		else if(tarea.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo area!</div>";
			return false;
		}	
		else if(tordencompra.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Orden de Compra!</div>";
			return false;
		}	
		else if(tfecha.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Fecha de adquisicion!</div>";
			return false;
		}	
		else if(tfecha2.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Fecha de Expedicion de Garantia!</div>";
			return false;
		}	
		else if(tcantidad.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo cantidad!</div>";
			return false;
		}
		else if(tcosto.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo costo!</div>";
			return false;
		}
		else if(tcomentario.value=="")
		{  
	        alertasArticulo.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo comentario!</div>";
			return false;
		}		
		else {return true;}	
	}

	function validarRegistroProveedor(){
		if(tcodigo2.value=="")
		{  
	        alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo codigo!</div>";
			return false;
		}		
		
		else if(tdescripcion2.value=="")
		{  
	        alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo descripcion!</div>";
			return false;
		}
		
		else if(tresponsable.value=="")
		{  
	        alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Responsable!</div>";
			return false;
		}
		else if(tUbicacion.value=="")
		{  
	        alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Ubicacion!</div>";
			return false;
		}
		else if(ttelefono.value=="")
		{  
	        alertasProveedor.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Telefono!</div>";
			return false;
		}		
		
		else {return true;}	
	}
function validarRegistroUsuario(){
	if(tcedula.value==""){
		alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo cedula!</div>";
		return false;
	}	
	else if(tnombre.value=="")
	{  
        alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo Nombre!</div>";
		return false;
	}	
	
	else if(tcorreo.value==""||!validar_email(tcorreo.value))
	{  
        alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo correo con uno valido!</div>";
		return false;
	}
	else if(tpass.value=="")
	{  
        alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo contraseña !</div>";
		return false;
	}
	else if(ttelefono.value=="")
	{  
        alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo telefono!</div>";
		return false;
	}	
	else if(tdireccion.value=="")
	{  
        alertasUsuario.innerHTML="<div class='alert alert-warning' role='alert'>¡Por Favor llene el campo direccion!</div>";
		return false;
	}		
	
	else {return true;}	
}


	function validar_email(valor)
	{
		// creamos nuestra regla con expresiones regulares.
		var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
		// utilizamos test para comprobar si el parametro valor cumple la regla
		if(filter.test(valor))
			return true;
		else
			return false;
	}